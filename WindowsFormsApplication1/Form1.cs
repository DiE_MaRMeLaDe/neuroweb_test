﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FANNCSharp;
#if FANN_FIXED
using FANNCSharp.Fixed;
using DataType = System.Int32;
#elif FANN_DOUBLE
using FANNCSharp.Double;
using DataType = System.Double;
#else
using FANNCSharp.Float;
using DataType = System.Single;
#endif
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Bitmap first;
        Bitmap second;
        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Укажите 1 картинку";
            openFileDialog1.ShowDialog();
            first = Bitmap.FromFile(openFileDialog1.FileName) as Bitmap;
            pictureBox1.Image = first;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Укажите 2 картинку";
            openFileDialog1.ShowDialog();
            second = Bitmap.FromFile(openFileDialog1.FileName) as Bitmap;
            pictureBox2.Image = second;
            
        }
        float[][] traindata = new float[2][];
        private void button2_Click(object sender, EventArgs e)
        {
            /*traindata[0] = new float[2] {255, 0 };
            traindata[1] = new float[2] { 0, 0 };
            traindata[2] = new float[2] { 0, 255 };
            traindata[3] = new float[2] { 255, 255 };*/
               traindata[0] = new float[first.Width * first.Height];
               traindata[1] = new float[second.Width * second.Height];
            //записываем красный канал каждого пикселя в массив
            for (int y = 1; y < first.Size.Height; y++) //первая картинка
                
               {
                   for (int x = 1; x < first.Size.Width; x++)
                   {
                       traindata[0][x + y - 2] = first.GetPixel(x, y).R / 255;
                   }
               }

               for (int y = 1; y < second.Size.Height; y++) // вторая
               {
                   for (int x = 1; x < second.Size.Width; x++)
                   {
                       traindata[1][x + y - 2] = second.GetPixel(x, y).R / 255;
                   }
               } 

            float[][] trainOutput = new float[2][];
            trainOutput[0] = new float[1] { 1 }; // какие выходные значения должны быть
            trainOutput[1] = new float[1] { 0 };
            CreateAndTrainNet(traindata, trainOutput); //создаем и обучаем сеть
        }

        private void button4_Click(object sender, EventArgs e)
        {

#if FANN_FIXED
            using (NeuralNet net = new NeuralNet("..\\..\\..\\examples\\xor_fixed.net"))
#else
                using (NeuralNet net = new NeuralNet("float.net"))
#endif
                {
                float[] data = new float[second.Width * second.Height];
                for (int y = 1; y < second.Size.Height; y++)  //записываем красный канал каждого пикселя в массив
                {
                    for (int x = 1; x < second.Size.Width; x++)
                    {
                        data[x + y - 2] = second.GetPixel(x, y).R / 255;
                    }
                }
                MessageBox.Show(net.Run(data)[0].ToString());
                }
            
        }


        static void CreateAndTrainNet(float[][] input, float[][] output)
        {
            const float learning_rate = 0.1f; //шаг, с каким учится сеть
            const uint num_layers = 3; // количество слоев в сети
            uint num_input = (uint)input[0].Count(); //количество входных значений
             uint num_hidden = (uint)input[0].Count() + 2; // количество спрятанных узлов
            const uint num_output = 1; // количество выходных значений
            const float desired_error = 0.001f; // допустимая ошибка
            const uint max_iterations = 1000; //максимальное количество эпох(циклов тренеровки)
            const uint iterations_between_reports = 1000;

            Console.WriteLine("\nCreating network.");

            using (NeuralNet net = new NeuralNet(NetworkType.LAYER, num_layers, num_input, num_hidden, num_output))
            {
                net.LearningRate = learning_rate;
                net.ActivationSteepnessHidden = 1.0F;
                net.ActivationSteepnessOutput = 1.0F;

                net.ActivationFunctionHidden = ActivationFunction.SIGMOID; 
                net.ActivationFunctionOutput = ActivationFunction.SIGMOID;

                Console.WriteLine("\nTraining network.");

                using (TrainingData data = new TrainingData())
                {
                        data.SetTrainData(input, output);
                        // инициализация и тренеровка
                        net.InitWeights(data);
                        net.TrainOnData(data, max_iterations, iterations_between_reports, desired_error);
                        for (uint i = 0; i < data.TrainDataLength; i++)
                        {
                            net.Run(data.Input[i]);
                    }
                        // сохраняем сеть
                        net.Save("float.net");
                        uint decimal_point = (uint)net.SaveToFixed("fixed.net");
                        data.SaveTrainToFixed("fixed.data", decimal_point);
                }
            }
        }
    }
}
